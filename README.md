# class-scheduler

- The program is fully configurable by using external configuration file.
- Configuration is externalized using `application.yml` configuration file.
- Configuration file name and input file can be passed externally by using `-Dinput` and `-Dconfig` system properties.
- To run the jar file with external configuration kindly execute `run-with-default-config.bat`.
- To run the jar file with default configuration kindly execute `run.bat`.
- `java -jar -Dinput="input-external.json" -Dconfig="application-external.yml" class-scheduler-0.0.1-SNAPSHOT.jar`.
- 90% of code coverage with zero sonar issues.

### Application Design

- Application flow starts with `ClassSchedulerApplication.java`.
- `ServiceFactory` is responsible for getting the instances of both the services `SchedulerService` and `SlotService`.
- `SchedulerService` is responsible for generating schedule given by the `SlotService`.
- `SlotService` is responsible for finding out the next available slot and maintain the in memory map of allocated
  slots.
- `OutputHandler` is responsible for converting the `Schedule` and `Slot` into formatted output `String`.
- `InputHandler` is responsible for handling input and returns the java object.
- `ObjectMapperFactory` is responsible for returning the `ObjectMapper` used by `YmlConverter` and `JsonConverter`.
- Other static singleton classes like `IOUtils` and `DateUtil` is being used throughout the application as a helper
  classes.
- The package `com.jigarnaik.classscheduler.predicate` contains `Predicates` which are used to determine if the given
  slot is valid for the gym member.
- More predicates can be added without modifying existing code to add any other constraint.
- `com.jigarnaik.classscheduler.model` are the pojo classes which are mapping for application.yml configuration file.

### External Configuration Details and supported values.

      #The day from which schedule needs to be generated. It can be any day except offDays value.
    weekStartDay: MONDAY

      #No of days for which schedule needs to be generated, which means the application can generate schedule for any no of days as long as hardware resouces are sufficient.
    scheduledDays: 5

      #Off days can be configured below, without making any modification in code.
    offDays:   
        - SATURDAY
        - SUNDAY

     #In future in case the gym plans to add another slot, new configuration can be added over here without making any code change.
     # Possible values for slotType are (MORNING, AFTERNOON, EVENING, NIGHT, AFTER_LUNCH, AFTER_OFFICE) controlled by SlotType enum.
    timeOfDays: 
        - slotType: MORNING   
          startTime: 11:00
          endTime: 13:00
        - slotType: AFTER_OFFICE  
          startTime: 18:00
          endTime: 20:00

       #In future if the gym plans to add more room, room can be added over here without making any code change.
    rooms:  
      - Red
      - Blue

    NODE: Due to time constraint I have not been able to validate the values of external configuration shown above,
            If the time permits I would have used javax.validation annotations to validate the configuration as well as input. 

### Language and Library used.

* `Java11`
* `Maven`
* `JUnit5`
* `Mockito`
* `Log4j2`
* `Jackson`

Contact: jigarsnaik@gmail.com 