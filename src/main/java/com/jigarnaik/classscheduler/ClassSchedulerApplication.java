package com.jigarnaik.classscheduler;

import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.service.InputHandler;
import com.jigarnaik.classscheduler.service.OutputHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;

public class ClassSchedulerApplication {

    private static final Logger LOGGER = LogManager.getLogger(ClassSchedulerApplication.class);

    public static void main(String[] args) {
        try {
            ServiceFactory.getSchedulerService().generateSchedule(new InputHandler<>(Member.class).parseInput());
            OutputHandler handler = new OutputHandler();
            LOGGER.info("Scheduled : \n{}", handler.getFormattedScheduled(ServiceFactory.getSchedulerService().getScheduled()));
            LOGGER.info("Non Scheduled : \n{}", handler.getFormattedNonScheduled(ServiceFactory.getSchedulerService().getNonScheduled()));
            LOGGER.info("Available Slots : \n{}", handler.getFormattedAvailableSlots(ServiceFactory.getSlotService().getAvailableSlots()));
        } catch(FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }

    }
}
