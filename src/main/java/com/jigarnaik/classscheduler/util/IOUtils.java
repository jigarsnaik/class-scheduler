package com.jigarnaik.classscheduler.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class IOUtils {

    private static final ClassLoader classLoader = IOUtils.class.getClassLoader();

    private IOUtils() {
    }

    public static InputStream getInputStream(String fileName) {
        return classLoader.getResourceAsStream(fileName);
    }

    public static URL getExternalURL(String fileName) throws MalformedURLException, FileNotFoundException {
        File file = new File(fileName);
        URL url = file.toURI().toURL();
        verifyIfFileExist(fileName, url);
        return url;
    }

    private static void verifyIfFileExist(String fileName, URL url) throws FileNotFoundException {
        if (Objects.isNull(url) || !new File(url.getPath()).exists()) {
            throw new FileNotFoundException("File not found, file name : " + fileName + ", url : " + url);
        }
    }

}
