package com.jigarnaik.classscheduler.util;

import com.jigarnaik.classscheduler.factory.ObjectMapperFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static com.jigarnaik.classscheduler.constants.ObjectMapperType.YML;

public class YmlConverter {

    private YmlConverter() {
    }

    public static <T> T toObject(InputStream inputStream, Class<T> clazz) throws IOException {
        return ObjectMapperFactory.getObjectMapper(YML).readValue(inputStream, clazz);
    }

    public static <T> T toObject(URL url, Class<T> clazz) throws IOException {
        return ObjectMapperFactory.getObjectMapper(YML).readValue(url, clazz);
    }
}
