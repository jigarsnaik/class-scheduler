package com.jigarnaik.classscheduler.util;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Objects;

public class DateUtil {

    public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("E, MMM dd, yyyy HH:mm a");

    private DateUtil() {

    }

    public static LocalDate getNextDay(LocalDate date, int days, List<DayOfWeek> offDays) {
        LocalDate result = date;
        int addedDays = 0;
        while (addedDays < days) {
            result = result.plusDays(1);
            if (!offDays.contains(result.getDayOfWeek())) {
                ++addedDays;
            }
        }
        return result;
    }

    public static LocalDate getNextDayOfWeek(DayOfWeek dayOfWeek) {
        if (Objects.isNull(dayOfWeek)) return null;
        LocalDate date = LocalDate.now();
        return date.with(TemporalAdjusters.next(dayOfWeek));
    }

    public static long getDuration(LocalDateTime startTime, LocalDateTime endTime) {
        return Duration.between(startTime, endTime).toMinutes();
    }
}
