package com.jigarnaik.classscheduler.util;

import com.fasterxml.jackson.databind.JavaType;
import com.jigarnaik.classscheduler.factory.ObjectMapperFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import static com.jigarnaik.classscheduler.constants.ObjectMapperType.JSON;

public class JsonConverter {

    private JsonConverter() {
    }

    public static <T> List<T> toObjectList(URL url, Class<T> clazz) throws IOException {
        JavaType type = ObjectMapperFactory.getObjectMapper(JSON).getTypeFactory().constructCollectionType(List.class, clazz);
        return ObjectMapperFactory.getObjectMapper(JSON).readValue(url, type);
    }

    public static <T> List<T> toObjectList(InputStream inputStream, Class<T> clazz) throws IOException {
        JavaType type = ObjectMapperFactory.getObjectMapper(JSON).getTypeFactory().constructCollectionType(List.class, clazz);
        return ObjectMapperFactory.getObjectMapper(JSON).readValue(inputStream, type);
    }

}
