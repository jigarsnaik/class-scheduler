package com.jigarnaik.classscheduler.exception;

public class ConfigLoadException extends RuntimeException{

    public ConfigLoadException(String message) {
        super(message);
    }
}
