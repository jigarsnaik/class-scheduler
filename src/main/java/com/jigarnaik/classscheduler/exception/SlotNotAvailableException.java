package com.jigarnaik.classscheduler.exception;

public class SlotNotAvailableException extends Exception {

    public SlotNotAvailableException(String message) {
        super(message);
    }
}
