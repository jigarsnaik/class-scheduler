package com.jigarnaik.classscheduler.factory;

import com.jigarnaik.classscheduler.service.SchedulerService;
import com.jigarnaik.classscheduler.service.SlotService;

public class ServiceFactory {

    private ServiceFactory() {

    }

    public static SlotService getSlotService() {
        return SlotService.getInstance1();
    }

    public static SchedulerService getSchedulerService() {
        return SchedulerService.getInstance();
    }
}
