package com.jigarnaik.classscheduler.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jigarnaik.classscheduler.constants.ObjectMapperType;

import java.util.EnumMap;

public class ObjectMapperFactory {

    private static final EnumMap<ObjectMapperType, ObjectMapper> CACHE = new EnumMap<>(ObjectMapperType.class);

    static {
        CACHE.put(ObjectMapperType.JSON, new ObjectMapper().registerModule(new JavaTimeModule()).registerModule(new Jdk8Module()));
        CACHE.put(ObjectMapperType.YML, new ObjectMapper(new YAMLFactory()).registerModule(new JavaTimeModule()).registerModule(new Jdk8Module()));
    }

    private ObjectMapperFactory() {
    }

    public static ObjectMapper getObjectMapper(ObjectMapperType objectMapperType) {
        return CACHE.get(objectMapperType);
    }
}
