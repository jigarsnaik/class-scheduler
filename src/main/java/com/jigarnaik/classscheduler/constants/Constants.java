package com.jigarnaik.classscheduler.constants;

public class Constants {

    private Constants() {

    }
    public static final String BOOKED_SLOTS = " %-15s %-30s %-15s %-30s %-25s %-30s \n";
    public static final String BOOKING_FAILURE = " Could not book %s session(s) for %s with trainer %s for member %s.\n";
    public static final String PENDING_SLOTS = " %-15s %-20s %-30s %-30s %-20s\n";
    public static final String NO_SCHEDULE_FOUND = "No schedule found.";
    public static final String NO_PENDING_SCHEDULE = "Everything is scheduled, no pending schedule.\n";
    public static final String FULLY_BOOKED = "fully booked";
    public static final String MINUTES = " minutes";
    public static final String APPLICATION_CONFIG_FILE = "application.yml";
    public static final String INPUT_FILE = "input.json";
    public static final String SYSTEM_PROPERTY_INPUT = "input";
    public static final String SYSTEM_PROPERTY_CONFIG = "config";



}
