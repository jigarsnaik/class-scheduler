package com.jigarnaik.classscheduler.constants;

public enum SlotType {
    MORNING("Morning"), AFTERNOON("Afternoon"), EVENING("Evening"), NIGHT("Night"), AFTER_LUNCH("After Lunch"), AFTER_OFFICE("After Office");

    private final String description;

    SlotType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
