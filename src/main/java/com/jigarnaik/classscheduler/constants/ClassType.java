package com.jigarnaik.classscheduler.constants;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ClassType {

    STRENGTH_AND_CONDITIONING(45, "Strength and Conditioning"),
    CARDIO(60, "Cardio"),
    MIND_AND_BODY(30, "Mind and Body"),
    DANCE(60, "Dance");

    private final int duration;
    private final String description;

    ClassType(int duration, String description) {
        this.duration = duration;
        this.description = description;
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

    public int getDuration() {
        return duration;
    }

}
