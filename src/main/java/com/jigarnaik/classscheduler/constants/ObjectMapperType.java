package com.jigarnaik.classscheduler.constants;

public enum ObjectMapperType {
    JSON, YML
}
