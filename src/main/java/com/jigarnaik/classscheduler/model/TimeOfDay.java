package com.jigarnaik.classscheduler.model;

import com.jigarnaik.classscheduler.constants.SlotType;

import java.time.LocalTime;

public class TimeOfDay {

    private SlotType slotType;
    private LocalTime startTime;
    private LocalTime endTime;

    public TimeOfDay() {
    }

    public TimeOfDay(SlotType slotType, LocalTime startTime, LocalTime endTime) {
        this.slotType = slotType;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "TimeOfDay{" +
                "slotType=" + slotType +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }

    public SlotType getSlotType() {
        return slotType;
    }

    public void setSlotType(SlotType slotType) {
        this.slotType = slotType;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }
}
