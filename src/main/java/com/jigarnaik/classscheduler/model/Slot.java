package com.jigarnaik.classscheduler.model;

import com.jigarnaik.classscheduler.constants.SlotType;
import com.jigarnaik.classscheduler.util.DateUtil;

import java.time.LocalDateTime;

public class Slot {
    private int id;
    private int day;
    private Room room;
    private LocalDateTime time;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private int timeRemaining;
    private SlotType slotType;

    public Slot(int id, int day, Room room, SlotType slotType, LocalDateTime startTime, LocalDateTime endTime, LocalDateTime time) {
        this.id = id;
        this.day = day;
        this.room = room;
        this.startTime = startTime;
        this.endTime = endTime;
        this.time = time;
        this.slotType = slotType;
    }

    public long getDuration() {
        return DateUtil.getDuration(startTime, endTime);
    }

    public long getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public SlotType getSlotType() {
        return slotType;
    }

    public void setSlotType(SlotType slotType) {
        this.slotType = slotType;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot slot = (Slot) o;
        return id == slot.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Slot reserve(int duration) {
        this.setTime(startTime);
        this.setStartTime(startTime.plusMinutes(duration));
        return this;
    }

}
