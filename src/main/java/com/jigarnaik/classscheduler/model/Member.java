package com.jigarnaik.classscheduler.model;

import com.jigarnaik.classscheduler.constants.ClassType;

import java.util.Objects;

public class Member {

    private String name;
    private ClassType type;
    private int frequency;
    private String trainer;
    private int allottedFrequency;

    public Member() {
    }

    public Member(String name, ClassType type, int frequency, String trainer) {
        this.name = name;
        this.type = type;
        this.frequency = frequency;
        this.trainer = trainer;
    }

    public int getAllottedFrequency() {
        return allottedFrequency;
    }

    public void setAllottedFrequency(int allottedFrequency) {
        this.allottedFrequency = allottedFrequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return name.equals(member.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Member{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", frequency=" + frequency +
                ", trainer='" + trainer + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClassType getType() {
        return type;
    }

    public void setType(ClassType type) {
        this.type = type;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getTrainer() {
        return trainer;
    }

    public void setTrainer(String trainer) {
        this.trainer = trainer;
    }

    public void allotFrequency() {
        this.allottedFrequency++;
    }

    public int getPendingSessions() {
        return this.frequency - this.allottedFrequency;
    }
}
