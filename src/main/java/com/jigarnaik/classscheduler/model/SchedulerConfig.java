package com.jigarnaik.classscheduler.model;

import com.jigarnaik.classscheduler.service.InputHandler;

import java.time.DayOfWeek;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SchedulerConfig {

    private static SchedulerConfig instance;
    private List<TimeOfDay> timeOfDays = Collections.emptyList();
    private DayOfWeek weekStartDay;
    private int scheduledDays;
    private List<Room> rooms = Collections.emptyList();
    private List<DayOfWeek> offDays = Collections.emptyList();

    private SchedulerConfig() {
    }

    public static SchedulerConfig getInstance() {
        if (instance == null) {
            instance = new InputHandler<>(SchedulerConfig.class).loadConfig();
        }
        return instance;
    }


    public int getScheduledDays() {
        return scheduledDays;
    }

    public void setScheduledDays(int scheduledDays) {
        this.scheduledDays = scheduledDays;
    }

    public DayOfWeek getWeekStartDay() {
        return weekStartDay;
    }

    public void setWeekStartDay(DayOfWeek weekStartDay) {
        this.weekStartDay = weekStartDay;
    }

    public List<DayOfWeek> getOffDays() {
        return offDays;
    }

    public void setOffDays(List<DayOfWeek> offDays) {
        this.offDays = offDays;
    }

    public List<Integer> getDays() {
        return IntStream.range(0, scheduledDays).boxed().collect(Collectors.toList());
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<TimeOfDay> getTimeOfDays() {
        return timeOfDays;
    }

    public void setTimeOfDays(List<TimeOfDay> timeOfDays) {
        this.timeOfDays = timeOfDays;
    }


}
