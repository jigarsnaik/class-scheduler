package com.jigarnaik.classscheduler.model;

public class Schedule {

    private Slot slot;
    private Member member;

    public Schedule(Slot slot, Member member) {
        this.slot = new Slot(slot.getId(), slot.getDay(), slot.getRoom(), slot.getSlotType(), slot.getStartTime(), slot.getEndTime(), slot.getTime());
        this.member = member;
    }

    public Schedule(Member member) {
        this.member = member;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

}
