package com.jigarnaik.classscheduler.predicate;

import com.jigarnaik.classscheduler.model.Slot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SlotExclusionPredicate {

    private static final List<Predicate<Slot>> predicates = new ArrayList<>();

    private static SlotExclusionPredicate instance;

    private SlotExclusionPredicate() {

    }

    public static SlotExclusionPredicate getInstance() {
        if (instance == null) {
            instance = new SlotExclusionPredicate();
            instance.register(new InsufficientDurationPredicate());
            instance.register(new ConflictingSlotPredicate());
        }
        return instance;
    }

    private void register(Predicate<Slot> predicate) {
        predicates.add(predicate);
    }

    public List<Predicate<Slot>> getPredicates() {
        return predicates;
    }
}
