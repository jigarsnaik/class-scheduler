package com.jigarnaik.classscheduler.predicate;

import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.model.Slot;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class ConflictingSlotPredicate implements Predicate<Slot> {

    @Override
    public boolean test(Slot availableSlot) {
        Map<Member, List<Slot>> allottedSlotsByMember = ServiceFactory.getSlotService().getAllottedSlotsByMember();
        List<Slot> allottedSlot = allottedSlotsByMember.getOrDefault(ServiceFactory.getSlotService().getMember(), Collections.emptyList());
        return allottedSlot.stream().noneMatch(slot -> slot.getDay() == availableSlot.getDay() && slot.getSlotType() == availableSlot.getSlotType());
    }

}
