package com.jigarnaik.classscheduler.predicate;

import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.Slot;

import java.util.function.Predicate;

public class InsufficientDurationPredicate implements Predicate<Slot> {

    @Override
    public boolean test(Slot availableSlot) {
        return availableSlot.getDuration() >= ServiceFactory.getSlotService().getMember().getType().getDuration();
    }
}
