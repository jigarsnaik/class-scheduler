package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.constants.Constants;
import com.jigarnaik.classscheduler.exception.ConfigLoadException;
import com.jigarnaik.classscheduler.util.IOUtils;
import com.jigarnaik.classscheduler.util.JsonConverter;
import com.jigarnaik.classscheduler.util.YmlConverter;
import org.apache.logging.log4j.util.Strings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static com.jigarnaik.classscheduler.constants.Constants.*;

public class InputHandler<T> {

    private final Class<T> type;

    public InputHandler(Class<T> clazz) {
        type = clazz;
    }

    public List<T> parseInput() throws FileNotFoundException {
        try {
            return Strings.isEmpty(System.getProperty(SYSTEM_PROPERTY_INPUT)) ?
                    JsonConverter.toObjectList(IOUtils.getInputStream(Constants.INPUT_FILE), type) :
                    JsonConverter.toObjectList(IOUtils.getExternalURL(System.getProperty(SYSTEM_PROPERTY_INPUT)), type);
        } catch (IOException e) {
            throw new FileNotFoundException("Exception while parsing input, " + System.getProperty("input"));
        }

    }

    public T loadConfig() {
        try {
            return Strings.isEmpty(System.getProperty(SYSTEM_PROPERTY_CONFIG)) ?
                    YmlConverter.toObject(IOUtils.getInputStream(APPLICATION_CONFIG_FILE), type) :
                    YmlConverter.toObject(IOUtils.getExternalURL(System.getProperty(SYSTEM_PROPERTY_CONFIG)), type);
        } catch (IOException e) {
            throw new ConfigLoadException("Could not load config file, " + System.getProperty("config"));
        }
    }
}
