package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.exception.SlotNotAvailableException;
import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.model.Schedule;

import java.util.ArrayList;
import java.util.List;

public class SchedulerService {

    private static SchedulerService instance;
    private final List<Schedule> scheduled = new ArrayList<>();
    private final List<Schedule> nonScheduled = new ArrayList<>();

    private SchedulerService() {

    }

    public static SchedulerService getInstance() {
        if (instance == null) {
            instance = new SchedulerService();
        }
        return instance;
    }

    public List<Schedule> getScheduled() {
        return scheduled;
    }

    public List<Schedule> getNonScheduled() {
        return nonScheduled;
    }

    public void generateSchedule(List<Member> members) {
        for (var member : members) {
            prepareMemberSchedule(member);
        }
    }

    private void prepareMemberSchedule(Member member) {
        for (int frequency = 1; frequency <= member.getFrequency(); frequency++) {
            try {
                var slot = ServiceFactory.getSlotService().getNextAvailableSlotFor(member);
                member.allotFrequency();
                scheduled.add(new Schedule(slot, member));
            } catch (SlotNotAvailableException e) {
                nonScheduled.add(new Schedule(member));
                break;
            }
        }
    }


}
