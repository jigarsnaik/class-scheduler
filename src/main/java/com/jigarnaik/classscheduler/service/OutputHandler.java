package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.model.Schedule;
import com.jigarnaik.classscheduler.model.Slot;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.jigarnaik.classscheduler.constants.Constants.*;
import static com.jigarnaik.classscheduler.util.DateUtil.DATE_TIME_FORMAT;

public class OutputHandler {

    public String getFormattedScheduled(List<Schedule> schedules) {
        if (schedules.isEmpty()) return NO_SCHEDULE_FOUND;
        StringBuilder output = new StringBuilder();
        Collections.sort(schedules, Comparator.comparingInt(s -> s.getSlot().getId()));
        output.append(String.format(BOOKED_SLOTS,"SLOT", "START TIME", "ROOM", "CLASS", "MEMBER NAME", "TRAINER"));
        for (Schedule s : schedules) {
            output.append(String.format(BOOKED_SLOTS, s.getSlot().getSlotType().getDescription(), DATE_TIME_FORMAT.format(s.getSlot().getTime()),
                    s.getSlot().getRoom().getName(), s.getMember().getType().getDescription(), s.getMember().getName(), s.getMember().getTrainer()));
        }
        return output.toString();
    }

    public String getFormattedNonScheduled(List<Schedule> schedules) {
        if (schedules.isEmpty()) return NO_PENDING_SCHEDULE;
        StringBuilder output = new StringBuilder();
        for (Schedule s : schedules) {
            output.append(String.format(BOOKING_FAILURE, s.getMember().getPendingSessions(), s.getMember().getType().getDescription(), s.getMember().getTrainer(), s.getMember().getName()));
        }
        return output.toString();
    }

    public String getFormattedAvailableSlots(List<Slot> slots) {
        StringBuilder output = new StringBuilder();
        output.append(String.format(PENDING_SLOTS,"ROOM", "SLOT","AVAILABLE FROM","AVAILABLE TO","MINUTES"));
        for (Slot s : slots) {
            output.append(String.format(PENDING_SLOTS,s.getRoom().getName(), s.getSlotType().getDescription(), DATE_TIME_FORMAT.format(s.getStartTime()),
                    DATE_TIME_FORMAT.format(s.getEndTime()), s.getDuration() == 0 ? FULLY_BOOKED : s.getDuration() + MINUTES));
        }
        return output.toString();

    }

}
