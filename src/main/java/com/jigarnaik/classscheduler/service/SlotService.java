package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.exception.SlotNotAvailableException;
import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.model.SchedulerConfig;
import com.jigarnaik.classscheduler.model.Slot;
import com.jigarnaik.classscheduler.predicate.SlotExclusionPredicate;
import com.jigarnaik.classscheduler.util.DateUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class SlotService {

    private static SlotService instance;
    private final Map<Member, List<Slot>> allottedSlotsByMember = new HashMap<>();
    private final List<Slot> availableSlots = new ArrayList<>();
    private Member member;

    private SlotService() {
        createEmptySlots();
    }

    public static SlotService getInstance1() {
        if (instance == null) {
            instance = new SlotService();
        }
        return instance;
    }

    public List<Slot> getAvailableSlots() {
        return availableSlots;
    }

    private List<Slot> createEmptySlots() {
        SchedulerConfig schedulerConfig = SchedulerConfig.getInstance();
        LocalDate initDate = DateUtil.getNextDayOfWeek(schedulerConfig.getWeekStartDay());
        int slotId = 0;
        for (var day : schedulerConfig.getDays()) {
            for (var timeOfDay : schedulerConfig.getTimeOfDays()) {
                for (var room : schedulerConfig.getRooms()) {
                    var startDateTime = LocalDateTime.of(DateUtil.getNextDay(initDate, day, schedulerConfig.getOffDays()), timeOfDay.getStartTime());
                    var endDateTime = LocalDateTime.of(DateUtil.getNextDay(initDate, day, schedulerConfig.getOffDays()), timeOfDay.getEndTime());
                    availableSlots.add(new Slot(slotId++, day, room, timeOfDay.getSlotType(), startDateTime, endDateTime, startDateTime));
                }
            }
        }
        return availableSlots;
    }

    public Slot getNextAvailableSlotFor(Member member) throws SlotNotAvailableException {
        this.member = member;
        Slot chosenSlot = availableSlots.stream()
                .filter(SlotExclusionPredicate.getInstance().getPredicates().stream().reduce(x -> true, Predicate::and))
                .findFirst().orElseThrow(() -> new SlotNotAvailableException("No slots available."));
        chosenSlot.reserve(member.getType().getDuration());
        allottedSlotsByMember.computeIfAbsent(member, e -> new ArrayList<>()).add(chosenSlot);
        return chosenSlot;
    }

    public Map<Member, List<Slot>> getAllottedSlotsByMember() {
        return allottedSlotsByMember;
    }

    public Member getMember() {
        return member;
    }


}
