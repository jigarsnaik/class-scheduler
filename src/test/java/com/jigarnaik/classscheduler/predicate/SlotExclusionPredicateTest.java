package com.jigarnaik.classscheduler.predicate;

import com.jigarnaik.classscheduler.model.Slot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Predicate;

class SlotExclusionPredicateTest {

    @BeforeEach
    public void setUp() {
    }

    @Test
    void getPredicates() {
        List<Predicate<Slot>> predicates = SlotExclusionPredicate.getInstance().getPredicates();
        Assertions.assertEquals(2, predicates.size());
    }
}