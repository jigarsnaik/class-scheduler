package com.jigarnaik.classscheduler.predicate;

import com.jigarnaik.classscheduler.constants.ClassType;
import com.jigarnaik.classscheduler.constants.SlotType;
import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.model.Room;
import com.jigarnaik.classscheduler.model.Slot;
import com.jigarnaik.classscheduler.service.SlotService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

class InsufficientDurationPredicateTest {

    private InsufficientDurationPredicate predicate;

    private static Stream<Arguments> testDataProvider() {
        return Stream.of(

                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:00"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.CARDIO, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), true),

                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:00"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.DANCE, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), true),


                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:30"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.MIND_AND_BODY, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), true),


                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:15"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.STRENGTH_AND_CONDITIONING, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), true),

                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:01"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.CARDIO, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), false),

                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:01"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.DANCE, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), false),


                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:31"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.MIND_AND_BODY, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), false),


                Arguments.of(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T12:16"), LocalDateTime.parse("2021-06-02T13:00"), null),
                        new Member("Jigar Naik", ClassType.STRENGTH_AND_CONDITIONING, 5, "Aura"),
                        Arrays.asList(new Slot(1, 1, new Room("RED"), SlotType.MORNING, LocalDateTime.parse("2021-06-02T11:00"), LocalDateTime.parse("2021-06-02T12:00"), null)), false)

        );
    }

    @ParameterizedTest
    @MethodSource("testDataProvider")
    void test1(Slot slotToTest, Member member, List<Slot> allottedSlots, boolean expected) {
        SlotService slotServiceMock = Mockito.mock(SlotService.class);
        try (MockedStatic<ServiceFactory> serviceFactoryMock = Mockito.mockStatic(ServiceFactory.class)) {
            serviceFactoryMock.when(() -> ServiceFactory.getSlotService()).thenReturn(slotServiceMock);
            Mockito.when(slotServiceMock.getAllottedSlotsByMember()).thenReturn(Map.of(member, allottedSlots));
            Mockito.when(slotServiceMock.getMember()).thenReturn(member);
            predicate = new InsufficientDurationPredicate();
            Assertions.assertEquals(expected, predicate.test(slotToTest));
        }



    }

}