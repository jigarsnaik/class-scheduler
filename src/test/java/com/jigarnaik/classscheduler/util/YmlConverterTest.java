package com.jigarnaik.classscheduler.util;

import com.jigarnaik.classscheduler.model.SchedulerConfig;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.DayOfWeek;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class YmlConverterTest {

    @Test
    void toObject() throws IOException {
        SchedulerConfig schedulerConfig = YmlConverter.toObject(IOUtils.getInputStream("application.yml"), SchedulerConfig.class);
        assertNotNull(schedulerConfig);
        assertEquals(5, schedulerConfig.getDays().size());
        assertEquals(DayOfWeek.MONDAY, schedulerConfig.getWeekStartDay());
        assertEquals(2, schedulerConfig.getTimeOfDays().size());
        assertEquals(2, schedulerConfig.getRooms().size());
        assertEquals(2, schedulerConfig.getOffDays().size());
        assertEquals(5, schedulerConfig.getScheduledDays());
    }
}