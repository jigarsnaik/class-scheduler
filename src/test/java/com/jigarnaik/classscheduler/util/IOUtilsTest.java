package com.jigarnaik.classscheduler.util;

import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class IOUtilsTest {

    @Test
    void getURL() {
        URL url = assertDoesNotThrow(() -> IOUtils.getExternalURL("application-external.yml"));
        assertNotNull(url);
    }

}