package com.jigarnaik.classscheduler.util;

import com.jigarnaik.classscheduler.model.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

class JsonConverterTest {

    @Test
    void toObjectList() throws IOException {
        List<Member> members = JsonConverter.toObjectList(IOUtils.getInputStream("input.json"), Member.class);
        Assertions.assertEquals(3, members.size());
    }
}