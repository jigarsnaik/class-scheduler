package com.jigarnaik.classscheduler.util;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DateUtilTest {

    @Test
    void getNextDay() {
        LocalDate nextDay0 = DateUtil.getNextDay(LocalDate.of(2021, 10, 3), 0, List.of(DayOfWeek.SUNDAY, DayOfWeek.SATURDAY, DayOfWeek.MONDAY));
        assertEquals("2021-10-03", nextDay0.toString());

        LocalDate nextDay1 = DateUtil.getNextDay(LocalDate.of(2021, 10, 3), 1, List.of(DayOfWeek.SUNDAY, DayOfWeek.SATURDAY, DayOfWeek.MONDAY));
        assertEquals("2021-10-05", nextDay1.toString());
    }

    @Test
    void getNextDayOfWeek() {
        LocalDate dateTime = LocalDate.of(2021, 10, 2);
        try (MockedStatic<LocalDate> localDateMock = Mockito.mockStatic(LocalDate.class)) {
            localDateMock.when(() -> LocalDate.now()).thenReturn(dateTime);
            LocalDate nextDayOfWeek = DateUtil.getNextDayOfWeek(DayOfWeek.MONDAY);
            assertEquals("2021-10-04", nextDayOfWeek.toString());
        }
    }

    @Test
    void getDuration() {
        LocalDateTime start = LocalDateTime.parse("2021-10-03T11:00:00");
        LocalDateTime end = LocalDateTime.parse("2021-10-03T11:10:00");
        long duration = DateUtil.getDuration(start, end);
        assertEquals(10, duration);
    }
}