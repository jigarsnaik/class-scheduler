package com.jigarnaik.classscheduler.util;

import java.lang.reflect.Field;

public class TestUtil {

    public static void setFieldValue(Object instance, Class clazz, String fieldName, Object fieldValue) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(instance, fieldValue);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
