package com.jigarnaik.classscheduler.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jigarnaik.classscheduler.constants.ObjectMapperType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ObjectMapperFactoryTest {

    @Test
    void getObjectMapper() {
        ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper(ObjectMapperType.JSON);
        Assertions.assertNotNull(objectMapper);
        objectMapper = ObjectMapperFactory.getObjectMapper(ObjectMapperType.YML);
        Assertions.assertNotNull(objectMapper);

    }
}