package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.model.SchedulerConfig;
import com.jigarnaik.classscheduler.util.IOUtils;
import com.jigarnaik.classscheduler.util.JsonConverter;
import com.jigarnaik.classscheduler.util.YmlConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

class SchedulerServiceTest extends TestSupport {

    private OutputHandler handler;
    private SchedulerService schedulerService;

    private static Stream<Arguments> testDataProvider() {
        return Stream.of(
                Arguments.of("Lack of slots for single member", new String[]{"input-case-1.json", "application-config-case-1.yml"}, "expected-scheduled-case-1.txt", "expected-non-scheduled-case-1.txt", "expected-available-slots-case-1.txt"),
                Arguments.of("Lack of slots for multiple members", new String[]{"input-case-2.json", "application-config-case-2.yml"}, "expected-scheduled-case-2.txt", "expected-non-scheduled-case-2.txt", "expected-available-slots-case-2.txt"),
                Arguments.of("Test sample file", new String[]{"input-case-3.json", "application-config-case-3.yml"}, "expected-scheduled-case-3.txt", "expected-non-scheduled-case-3.txt", "expected-available-slots-case-3.txt"),
                Arguments.of("All slots fully booked", new String[]{"input-case-4.json", "application-config-case-4.yml"}, "expected-scheduled-case-4.txt", "expected-non-scheduled-case-4.txt", "expected-available-slots-case-4.txt")

        );
    }

    public static String getFileContent(String resourceFileName) throws IOException {
        File file = new File(SchedulerServiceTest.class.getClassLoader().getResource(resourceFileName).getFile());
        return Files.readString(file.toPath());
    }


    @BeforeEach
    public void setUp() {
        clearServiceInstances();
        Instant.now(Clock.fixed(
                Instant.parse("2018-08-22T10:00:00Z"),
                ZoneOffset.UTC));
        handler = new OutputHandler();
        schedulerService = ServiceFactory.getSchedulerService();
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("testDataProvider")
    void generateSchedule(String scenario, String[] args, String expectedScheduledFile, String expectedNonScheduledFile, String expectedAvailableSlotFile) throws IOException {
        LocalDate currentLocalDate = LocalDate.of(2021, 10, 3);
        try (MockedStatic<LocalDate> localDateMock = Mockito.mockStatic(LocalDate.class)) {
            try (MockedStatic<SchedulerConfig> schedulerConfigMock = Mockito.mockStatic(SchedulerConfig.class)) {

                SchedulerConfig schedulerConfig = YmlConverter.toObject(IOUtils.getInputStream(args[1]), SchedulerConfig.class);
                schedulerConfigMock.when(() -> SchedulerConfig.getInstance()).thenReturn(schedulerConfig);
                localDateMock.when(() -> LocalDate.now()).thenReturn(currentLocalDate);

                List<Member> member = JsonConverter.toObjectList(IOUtils.getInputStream(args[0]), Member.class);

                schedulerService.generateSchedule(member);

                Assertions.assertEquals(trim(getFileContent(expectedScheduledFile)), trim(handler.getFormattedScheduled(schedulerService.getScheduled())));
                Assertions.assertEquals(trim(getFileContent(expectedNonScheduledFile)), trim(handler.getFormattedNonScheduled(schedulerService.getNonScheduled())));
                Assertions.assertEquals(trim(getFileContent(expectedAvailableSlotFile)), trim(handler.getFormattedAvailableSlots(ServiceFactory.getSlotService().getAvailableSlots())));
            }
        }

    }

    private String trim(String str) {
        if (Objects.isNull(str)) return null;
        return str.replaceAll("^\\s+|\\s+$|\\s*(\n)\\s*|(\\s)\\s*", "$1$2").replace("\t", " ");
    }


}