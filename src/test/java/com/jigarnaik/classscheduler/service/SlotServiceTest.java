package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.constants.ClassType;
import com.jigarnaik.classscheduler.constants.SlotType;
import com.jigarnaik.classscheduler.exception.SlotNotAvailableException;
import com.jigarnaik.classscheduler.factory.ServiceFactory;
import com.jigarnaik.classscheduler.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.time.LocalTime;
import java.util.List;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SlotServiceTest extends TestSupport {

    private static final Logger LOGGER = LogManager.getLogger(SlotServiceTest.class);
    SchedulerConfig schedulerConfig;

    @BeforeEach
    public void setUp() {
        schedulerConfig = createSchedulerConfigForFourSlots();
    }

    @AfterEach
    public void tearDown() {

    }

    @Test
    @Order(1)
    void getAvailableSlots() {
        SchedulerConfig config = SchedulerConfig.getInstance();
        try (MockedStatic<SchedulerConfig> schedulerConfigMock = Mockito.mockStatic(SchedulerConfig.class)) {
            config.setOffDays(List.of(SUNDAY, SATURDAY));
            config.setWeekStartDay(SUNDAY);
            config.setScheduledDays(5);
            config.setRooms(List.of(new Room("Red"), new Room("Blue")));
            config.setTimeOfDays(List.of(new TimeOfDay(SlotType.MORNING, LocalTime.parse("11:00"), LocalTime.parse("13:00")), new TimeOfDay(SlotType.MORNING, LocalTime.parse("16:00"), LocalTime.parse("18:00"))));
            schedulerConfigMock.when(() -> SchedulerConfig.getInstance()).thenReturn(config);
            List<Slot> availableSlots = ServiceFactory.getSlotService().getAvailableSlots();
            Assertions.assertEquals(20, availableSlots.size());
        }
    }

    @Test
    @Order(2)
    void getNextAvailableSlotFor() {
        try (MockedStatic<SchedulerConfig> schedulerConfigMock = Mockito.mockStatic(SchedulerConfig.class)) {
            schedulerConfigMock.when(() -> SchedulerConfig.getInstance()).thenReturn(schedulerConfig);
            clearSlotServiceInstance();
            SlotService service = ServiceFactory.getSlotService();
            List<Slot> availableSlots = service.getAvailableSlots();
            Assertions.assertEquals(4, availableSlots.size());

            Member member = new Member("Jigar Naik", ClassType.CARDIO, 1, "Aura");
            Slot slot = assertDoesNotThrow(() -> service.getNextAvailableSlotFor(member));
            Assertions.assertNotNull(slot);
            Assertions.assertEquals(0, slot.getId());
            Assertions.assertEquals(SlotType.MORNING, slot.getSlotType());
            Assertions.assertEquals(0, slot.getDay());
            Assertions.assertEquals(60, slot.getDuration());
            Assertions.assertEquals("Red", slot.getRoom().getName());
            Assertions.assertEquals(1, member.getPendingSessions());
        }
    }

    @Test
    @Order(3)
    void getNextAvailableSlotFor_throwsExceptionWhenSlotNotAvailable() {
        try (MockedStatic<SchedulerConfig> schedulerConfigMock = Mockito.mockStatic(SchedulerConfig.class)) {
            schedulerConfigMock.when(() -> SchedulerConfig.getInstance()).thenReturn(schedulerConfig);
            clearSlotServiceInstance();
            SlotService service = ServiceFactory.getSlotService();
            List<Slot> availableSlots = service.getAvailableSlots();
            Assertions.assertEquals(4, availableSlots.size());
            Member member = new Member("Jigar Naik", ClassType.CARDIO, 3, "Aura");
            assertDoesNotThrow(() -> service.getNextAvailableSlotFor(member));
            SlotNotAvailableException exception = Assertions.assertThrows(SlotNotAvailableException.class, () -> service.getNextAvailableSlotFor(member));
            Assertions.assertEquals("No slots available.", exception.getMessage());
        }
    }

    @Test
    @Order(4)
    void getAvailableSlots_whenSchedulerConfigIsNotLoaded() {
        try (MockedStatic<SchedulerConfig> schedulerConfigMockedStatic = Mockito.mockStatic(SchedulerConfig.class)) {
            schedulerConfigMockedStatic.when(() -> SchedulerConfig.getInstance()).thenReturn(schedulerConfig);
            Assertions.assertEquals(4, ServiceFactory.getSlotService().getAvailableSlots().size());
        }
    }

    private SchedulerConfig createSchedulerConfigForFourSlots() {
        SchedulerConfig config = SchedulerConfig.getInstance();
        config.setOffDays(List.of(SUNDAY, SATURDAY));
        config.setWeekStartDay(SUNDAY);
        config.setScheduledDays(1);
        config.setRooms(List.of(new Room("Red"), new Room("Blue")));
        config.setTimeOfDays(List.of(
                new TimeOfDay(SlotType.MORNING, LocalTime.parse("11:00"), LocalTime.parse("13:00")),
                new TimeOfDay(SlotType.MORNING, LocalTime.parse("16:00"), LocalTime.parse("18:00"))));
        return config;
    }

}