package com.jigarnaik.classscheduler.service;

import com.jigarnaik.classscheduler.model.Member;
import com.jigarnaik.classscheduler.model.SchedulerConfig;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InputHandlerTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    @Order(1)
    void loadConfig() {
        InputHandler<SchedulerConfig> inputHandler = new InputHandler<>(SchedulerConfig.class);
        SchedulerConfig schedulerConfig = assertDoesNotThrow(() -> inputHandler.loadConfig());
        assertEquals(2, schedulerConfig.getOffDays().size());
        assertEquals(5, schedulerConfig.getDays().size());
        assertEquals(5, schedulerConfig.getScheduledDays());
        assertEquals(2, schedulerConfig.getRooms().size());
        assertEquals(2, schedulerConfig.getTimeOfDays().size());
    }

    @Test
    @Order(2)
    void parseInput() {
        InputHandler<Member> inputHandler = new InputHandler<>(Member.class);
        List<Member> members = assertDoesNotThrow(() -> inputHandler.parseInput());
        Assertions.assertEquals(3, members.size());
        for (Member member : members) {
            Assertions.assertNotNull(member);
            Assertions.assertNotNull(member.getType());
            Assertions.assertNotNull(member.getName());
            Assertions.assertNotNull(member.getTrainer());
            Assertions.assertTrue(member.getFrequency() > 0);
        }
    }

    @Test
    @Order(3)
    void parseInput_external() {
        System.setProperty("input", "input-external.json");
        InputHandler<Member> inputHandler = new InputHandler<>(Member.class);
        List<Member> members = assertDoesNotThrow(() -> inputHandler.parseInput());
        Assertions.assertEquals(1, members.size());
        for (Member member : members) {
            Assertions.assertNotNull(member);
            Assertions.assertNotNull(member.getType());
            Assertions.assertNotNull(member.getName());
            Assertions.assertNotNull(member.getTrainer());
            Assertions.assertTrue(member.getFrequency() > 0);
        }
    }


    @Test
    @Order(4)
    void loadConfig_loadExternalConfig() {
        System.setProperty("config", "application-external.yml");
        InputHandler<SchedulerConfig> inputHandler = new InputHandler<>(SchedulerConfig.class);
        SchedulerConfig schedulerConfig = assertDoesNotThrow(() -> inputHandler.loadConfig());
        assertEquals(2, schedulerConfig.getOffDays().size());
        assertEquals(3, schedulerConfig.getDays().size());
        assertEquals(3, schedulerConfig.getScheduledDays());
        assertEquals(1, schedulerConfig.getRooms().size());
        assertEquals(1, schedulerConfig.getTimeOfDays().size());
    }

}