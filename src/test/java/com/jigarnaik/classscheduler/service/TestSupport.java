package com.jigarnaik.classscheduler.service;

import java.lang.reflect.Field;

public abstract class TestSupport {

    protected void clearSlotServiceInstance() {
        try {
            Field field = SlotService.class.getDeclaredField("instance");
            field.setAccessible(true);
            field.set(null, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void clearSchedulerServiceInstance() {
        try {
            Field field = SchedulerService.class.getDeclaredField("instance");
            field.setAccessible(true);
            field.set(null, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void clearServiceInstances() {
        clearSchedulerServiceInstance();
        clearSlotServiceInstance();
    }
}
