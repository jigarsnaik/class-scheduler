package com.jigarnaik.classscheduler;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ClassSchedulerApplicationTest {

    @Test
    void main() {
        Assertions.assertDoesNotThrow(() -> ClassSchedulerApplication.main(new String[]{}));
    }

}